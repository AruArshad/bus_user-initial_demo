var app = angular.module('starter.main_controller', []);

app.controller('AppCtrl', function($rootScope, $scope, $state, $ionicHistory, $stateParams) {

  $rootScope.icon = {
    disable: true,
    show: true,
    icon: 'icon ion-home'
  };

  //change this to your local server if you are in development
  //change this to main server for release
  $rootScope.url = "http://192.168.8.103:8080/tt/";

  $rootScope.profile = "features/common/image/avatar-1577909.svg";
  // $rootScope.bus = "features/common/image/MTS_Bus_icon.png";
  $rootScope.name = "Default";

  $scope.icon = $rootScope.icon;

  $scope.goBack = function() {
    $ionicHistory.goBack();
  };

  $scope.clearFabs = function() {
    var fabs = document.getElementsByClassName('main-fab');
    if (fabs.length && fabs.length > 1) {
      fabs[0].remove();
    }
  };

  $scope.$on("$ionicView.enter", function(event, data) {
    //  console.log(data.stateName);
    if (data.stateName == "eventmenu.busEdit") {
      $rootScope.icon.disable = false;
      $rootScope.icon.show = true;
      $rootScope.icon.icon = 'ion-levels';
    } else {
      $rootScope.icon.disable = true;
      $rootScope.icon.show = false;
    }
  });

});

// app.directive('autoNext', function() {
//   return {
//     restrict: 'A',
//     link: function(scope, element, attr, pinCode) {
//       var tabindex = parseInt(attr.tabindex);
//       var maxLength = parseInt(attr.ngMaxlength);
//       element.on('keypress', function(e) {
//         if (element.val().length > maxLength - 1) {
//           var next = angular.element(document.body).find('[tabindex=' + (tabindex + 1) + ']');
//           if (next.length > 0) {
//             next.focus();
//             return next.triggerHandler('keypress', {
//               which: e.which
//             });
//           } else {
//             return false;
//           }
//         }
//         return true;
//       });
//
//     }
//   }
// });
