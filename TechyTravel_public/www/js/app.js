// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngResource', 'ionic-material', 'ngMessages',
    'starter.main_controller',
    'starter.homeCtrl'
  ])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider) {
    /**
     * All State codes are here
     */
    $stateProvider
      .state('eventmenu', {
        url: '/event',
        abstract: true,
        templateUrl: 'views/side_menu.html',
        controller: 'AppCtrl'
      })

      .state('eventmenu.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'features/Home/home.html',
            controller: 'ViewHomeCtrl'
          },
          'fabContent': {
            template: '<button id="home-activity" ui-sref="eventmenu.homeAdd" class="button button-fab button-fab-bottom-right button-energized-900 main-fab" style="display: none";"><i class="icon ion-plus"></i></button>',
            controller: 'ViewHomeCtrl'

          }
        }
      });

    $urlRouterProvider.otherwise('/event/home');

  })

  /**
   *  Slide Menu Controller
   */
  .controller('MainCtrl', function($scope, ionicMaterialInk, ionicMaterialMotion, $ionicSideMenuDelegate, $timeout) {
    $timeout(function() {
      ionicMaterialInk.displayEffect();
      ionicMaterialMotion.ripple();
    }, 0);
  });
